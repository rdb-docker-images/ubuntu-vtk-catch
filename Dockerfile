FROM library/ubuntu:latest

ENV DEBIAN_FRONTEND noninteractive

MAINTAINER "Ruben Di Battista" <rubendibattista@gmail.com>

RUN apt-get -y update && \
    apt-get -y install build-essential catch git cmake vtk7 libvtk7-dev libvtk7.1 valgrind
